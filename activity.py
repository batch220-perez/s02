# Activity

# [1]
year = int(input("Please input a year\n"))
if year > 0:
	if year % 4 == 0 and (year % 100 != 0 or year % 400 == 0):
		print(f"{year} is a leap year")
	else:
		print(f"{year} is not a leap year")
else:
	print(f"{year} is an invalid year")		



# [2]
row = int(input("Enter number of rows\n"))
column = int(input("Enter number of columns\n"))

for i in range(row):
    print('*' *column)


# c.perez 